package com.adidas.act.features;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class RyanairResultsPage extends PageObject {

    @FindBy(xpath = "//*[@id=\"booking-selection\"]/article/div[1]/div[1]/h1/translate/span[1]")
    private List<WebElement> resultOrigin;

    @FindBy(xpath = "//*[@id=\"booking-selection\"]/article/div[1]/div[1]/h1/translate/span[2]")
    private WebElement resultDestination;

    public RyanairResultsPage(WebDriver driver) {
        super(driver);
    }

    public List<String> getResultsList() {
        List<String> resultList = new ArrayList<>();
        for (WebElement element : resultOrigin) {
            resultList.add(element.getText());
        }
        return resultList;
    }
}
